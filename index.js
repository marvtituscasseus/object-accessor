module.exports = function objectAccessor( inputObject ) {
    const _accessibleObject = inputObject;

    const get = function( key, defaultValue = '' ) {
        let value = defaultValue;

        if ( typeof( _accessibleObject[ key ] ) !== 'undefined' ) {
            value = _accessibleObject[ key ];
        }

        return value;
    };

    const getInt = function( key ) {
        let value = get( key, 0 );

        if ( isNaN( value )) {
            return 0;
        }

        return parseInt( value, 0 );
    };

    const getObject = function( key ) {
        return get( key, {} );
    };

    const getArray = function( key ) {
        return get( key, [] );
    };

    return {
        get: get,
        getInt: getInt,
        getObject: getObject,
        getArray: getArray,
    };
};